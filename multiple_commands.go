package main

import (
	"bytes"
	"fmt"
	"golang.org/x/crypto/ssh"
	"log"
	"strings"
)

//func main() {
func multipleCommands() {

	sshConfig := &ssh.ClientConfig{
		User: "root",
		Auth: []ssh.AuthMethod{
			ssh.Password("pa$$w0rd!"),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		//Timeout: 5 * time.Second,
	}

	conn, err := ssh.Dial("tcp", "10.201.48.22:22", sshConfig)
	logError2(err)
	defer conn.Close()

	session,err := conn.NewSession()
	logError2(err)
	defer session.Close()

	fmt.Println(1)
	// stdin pipe for shell commands
	stdin, err := session.StdinPipe()
	logError2(err)

	fmt.Println(2)
	//stdout,err := session.StdoutPipe()
	//if err != nil {
	//	log.Fatalln(err)
	//}
	//_, err = io.Copy(os.Stdout, stdout)
	//if err != nil {
	//	log.Fatal(err)
	//}

	//session.Stdout = os.Stdout

	//stderr, err := session.StderrPipe()
	//if err != nil {
	//	log.Fatalln(err)
	//}
	//io.Copy(os.Stderr, stderr)

	var stdoutBuf bytes.Buffer
	session.Stdout = &stdoutBuf

	fmt.Println(3)
	// remote shell
	err = session.Shell()
	logError2(err)

	commands := []string{
		"uptime",
		"pwd",
		"whoami",
		//"exit",
	}

	fmt.Println(4)
	for _, command := range commands {
		_, err := fmt.Fprintf(stdin, "%s\n", command)
		//_, err = stdin.Write([]byte(command + "\n"))
		logError2(err)
	}
	_, err = stdin.Write([]byte("exit\n"))
	logError2(err)

	err = session.Wait()
	logError2(err)

	results := strings.Split(stdoutBuf.String(), "\n")

	if len(results) > 0 {
		for _, res := range results[:len(results)-1] {
			fmt.Printf("Result: %v \n", res)
		}
	}

}

func logError2(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

