package main

import (
	"bytes"
	"fmt"
	"golang.org/x/crypto/ssh"
	"log"
)

//func main() {
func singleCommand() {

	sshConfig := &ssh.ClientConfig{
		User: "root",
		Auth: []ssh.AuthMethod{
			ssh.Password("pa$$w0rd!"),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		//Timeout: 5 * time.Second,
	}

	conn, err := ssh.Dial("tcp", "10.201.48.22:22", sshConfig)
	if err != nil {
		log.Fatalln(err)
	}
	defer conn.Close()

	session,err := conn.NewSession()
	if err != nil {
		log.Fatalln(err)
	}
	defer session.Close()

	//stdout,err := session.StdoutPipe()
	//if err != nil {
	//	log.Fatalln(err)
	//}
	//io.Copy(os.Stdout, stdout)
	//
	//stderr, err := session.StderrPipe()
	//if err != nil {
	//	log.Fatalln(err)
	//}
	//io.Copy(os.Stderr, stderr)

	var b bytes.Buffer
	session.Stdout = &b

	command := "uptime"

	if err = session.Run(command); err != nil {
		log.Fatalln(err)
	}

	fmt.Print(b.String())
}

