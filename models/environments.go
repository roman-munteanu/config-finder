package models

import (
	"database/sql"
	"errors"
)

type Environment struct {
	Id int64 `json:"id"`
	TeamId int64 `json:"teamId"`
	Name string `json:"name"`
}

type EnvModel struct {
	DB *sql.DB
}

const EnvFindAllStmt = "SELECT id, team_id, name FROM environments;"
const EnvFilterAllStmt = "SELECT id, team_id, name FROM environments WHERE team_id = ?;"
const EnvInsertStmt = "INSERT INTO environments (team_id, name) VALUES (?, ?);"
const EnvUpdateStmt = "UPDATE environments SET team_id = ?, name = ? WHERE id = ?;"
const EnvDeleteStmt = "DELETE FROM environments WHERE id = ?;"

func (model *EnvModel) FindAllEnvs() ([]*Environment, error) {

	rows, err := model.DB.Query(EnvFindAllStmt)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	result := make([]*Environment, 0)
	for rows.Next() {
		env := Environment{}
		err := rows.Scan(&env.Id, &env.TeamId, &env.Name)
		if err != nil {
			return nil, err
		}
		result = append(result, &env)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}

	return result, nil
}

func (model *EnvModel) FilterAllEnvs(teamId int64) ([]*Environment, error) {

	rows, err := model.DB.Query(EnvFilterAllStmt, teamId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	result := make([]*Environment, 0)
	for rows.Next() {
		env := Environment{}
		err := rows.Scan(&env.Id, &env.TeamId, &env.Name)
		if err != nil {
			return nil, err
		}
		result = append(result, &env)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}

	return result, nil
}

func (model *EnvModel) SaveEnv(env Environment) (*Environment, error) {

	var err error
	if env.Id != 0 {
		_, err = model.DB.Exec(EnvUpdateStmt, env.TeamId, env.Name, env.Id)
	} else {
		res, err := model.DB.Exec(EnvInsertStmt, env.TeamId, env.Name)
		if lastInsertId, err2 := res.LastInsertId(); err == nil && err2 == nil {
			env.Id = lastInsertId
		}
	}
	if err != nil {
		return &env, errors.New("500. Internal Server Error." + err.Error())
	}

	return &env, nil
}

func (model *EnvModel) DeleteEnv(id int64) error {
	if id != 0 {
		_, err := model.DB.Exec(EnvDeleteStmt, id)
		if err != nil {
			return errors.New("Error on delete operation")
		}
	}
	return nil
}