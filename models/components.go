package models

import (
	"database/sql"
	"errors"
	"strings"
)

type Component struct {
	Id int64 `json:"id"`
	TeamId int64 `json:"teamId"`
	EnvId int64 `json:"envId"`
	Name string `json:"name"`
	Protocol string `json:"protocol"`
	Port int16 `json:"port"`
	IpAddress string `json:"ipAddress"`
	DomainName string `json:"domainName"`
	StatusPath string `json:"statusPath"`
	HealthPath string `json:"healthPath"`
	Description string `json:"description"`
	Commands []*Command `json:"commands"`
	Errors map[string]string `json:"-"`
}

//func (c *Component) GetPort() interface{} {
//	if c.Port == 0 {
//		return nil
//	} else {
//		return c.Port
//	}
//}

func (c *Component) IsValid() bool {
	c.Errors = make(map[string]string)

	if strings.TrimSpace(c.Name) == "" {
		c.Errors["Name"] = "Name must not be blank"
	}

	return len(c.Errors) == 0
}

func (c *Component) GetErrorsStr() string {
	errSlice := make([]string, 0)
	for _, errMessage := range c.Errors {
		errSlice = append(errSlice, errMessage)
	}
	return strings.Join(errSlice, ",")
}

type ComponentsModel struct {
	DB *sql.DB
}

const ComponentsSearchStmt = "SELECT id, team_id, env_id, name, protocol, port, ip_address, domain_name, status_path, health_path, description FROM components WHERE team_id = ? AND env_id = ?;"

const ComponentsFindByIdStmt = "SELECT id, team_id, env_id, name, protocol, port, ip_address, domain_name, status_path, health_path, description FROM components WHERE id = ?;"

const ComponentsInsertStmt = "INSERT INTO components (team_id, env_id, name, protocol, port, ip_address, domain_name, status_path, health_path, description) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);"

const ComponentsUpdateStmt = "UPDATE components SET name=?, protocol=?, port=?, ip_address=?, domain_name=?, status_path=?, health_path=?, description=? WHERE id=?"

const ComponentsDeleteStmt = "DELETE FROM components WHERE id=?"

func (model *ComponentsModel) FilterAllComponents(teamId int64, envId int64) ([]*Component, error) {

	rows, err := model.DB.Query(ComponentsSearchStmt, teamId, envId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	result := make([]*Component, 0)
	for rows.Next() {
		c := Component{}
		err := rows.Scan(&c.Id, &c.TeamId, &c.EnvId, &c.Name, &c.Protocol, &c.Port, &c.IpAddress, &c.DomainName, &c.StatusPath, &c.HealthPath, &c.Description)
		if err != nil {
			return nil, err
		}
		result = append(result, &c)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}

	return result, nil
}

func (model *ComponentsModel) FindByIdComponent(componentId int64) (*Component, error) {

	row := model.DB.QueryRow(ComponentsFindByIdStmt, componentId)

	c := Component{}
	err := row.Scan(&c.Id, &c.TeamId, &c.EnvId, &c.Name, &c.Protocol, &c.Port, &c.IpAddress, &c.DomainName, &c.StatusPath, &c.HealthPath, &c.Description)
	if err != nil {
		return nil, err
	}

	return &c, nil
}

func (model *ComponentsModel) SaveComponent(c Component) (*Component, error) {

	var err error
	if c.Id != 0 {
		_, err = model.DB.Exec(ComponentsUpdateStmt, c.Name, c.Protocol, c.Port, c.IpAddress, c.DomainName, c.StatusPath, c.HealthPath, c.Description, c.Id)
	} else {
		res, err := model.DB.Exec(ComponentsInsertStmt, c.TeamId, c.EnvId, c.Name, c.Protocol, c.Port, c.IpAddress, c.DomainName, c.StatusPath, c.HealthPath, c.Description)
		if lastInsertId, err2 := res.LastInsertId(); err == nil && err2 == nil {
			c.Id = lastInsertId
		}
	}
	if err != nil {
		return &c, errors.New("500. Internal Server Error." + err.Error())
	}

	return &c, nil
}

func (model *ComponentsModel) DeleteComponent(id int64) error {

	if id != 0 {
		_, err := model.DB.Exec(ComponentsDeleteStmt, id)
		if err != nil {
			return errors.New("Error on delete operation")
		}
	}

	return nil
}