package models

import (
	"database/sql"
	"errors"
)

type Team struct {
	Id int64 `json:"id"`
	Name string `json:"name"`
}

type TeamsModel struct {
	DB *sql.DB
}

const TeamsFindAllStmt = "SELECT id, name FROM teams;"
const TeamsInsertStmt = "INSERT INTO teams (name) VALUES (?);"
const TeamsUpdateStmt = "UPDATE teams SET name = ? WHERE id = ?;"
const TeamsDeleteStmt = "DELETE FROM teams WHERE id = ?;"

func (model *TeamsModel) FindAllTeams() ([]*Team, error) {

	rows, err := model.DB.Query(TeamsFindAllStmt)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	result := make([]*Team, 0)
	for rows.Next() {
		t := Team{}
		err := rows.Scan(&t.Id, &t.Name)
		if err != nil {
			return nil, err
		}
		result = append(result, &t)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}

	return result, nil
}

func (model *TeamsModel) SaveTeam(t Team) (*Team, error) {

	var err error
	if t.Id != 0 {
		_, err = model.DB.Exec(TeamsUpdateStmt, t.Name, t.Id)
	} else {
		res, err := model.DB.Exec(TeamsInsertStmt, t.Name)
		if lastInsertId, err2 := res.LastInsertId(); err == nil && err2 == nil {
			t.Id = lastInsertId
		}
	}
	if err != nil {
		return &t, errors.New("500. Internal Server Error." + err.Error())
	}

	return &t, nil
}

func (model *TeamsModel) DeleteTeam(id int64) error {
	if id != 0 {
		_, err := model.DB.Exec(TeamsDeleteStmt, id)
		if err != nil {
			return errors.New("Error on delete operation")
		}
	}
	return nil
}