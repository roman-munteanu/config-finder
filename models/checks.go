package models

type Check interface {

}

type HealthCheck struct {
	Status string `json:"status"`
	Body string `json:"body"`
	Error string `json:"error"`
}