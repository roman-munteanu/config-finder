package models

import (
	"errors"
	"io/ioutil"
	"os"
)

type ConfigFile struct {
	ComponentId string `json:"componentId"`
	Name string `json:"name"`
	Body []byte `json:"body"`
}

func (cf *ConfigFile) GetFilePath(baseDir string) string {
	return baseDir + cf.ComponentId + "/" + cf.Name
}

type ConfigFileModel struct {

}

const BaseFilesDir = "static/files/"

func (model *ConfigFileModel) FilterAllConfigFiles(componentId string) ([]*ConfigFile, error) {

	result := make([]*ConfigFile, 0)

	dirPath := BaseFilesDir + componentId
	dir, err := os.Stat(dirPath)
	if err != nil {
		return result, err
	}
	if !dir.IsDir() {
		return result, errors.New("Directory does not exist: " + dirPath)
	}

	files, err := ioutil.ReadDir(dirPath)
	if err != nil {
		return nil, err
	}

	for _, fileInfo := range files {
		cf := ConfigFile{
			ComponentId: componentId,
			Name: fileInfo.Name(),
		}
		result = append(result, &cf)
	}

	return result, nil
}

func (model *ConfigFileModel) SaveConfigFile(cf ConfigFile) (*ConfigFile, error) {

	dirPath := BaseFilesDir + cf.ComponentId
	err := os.MkdirAll(dirPath, os.ModePerm)
	if err != nil {
		return nil, err
	}

	err = ioutil.WriteFile(cf.GetFilePath(BaseFilesDir), cf.Body, 0644)
	if err != nil {
		return nil, err
	}

	return &cf, nil
}

func (model *ConfigFileModel) DeleteConfigFile(cf ConfigFile) (*ConfigFile, error) {

	err := os.Remove(cf.GetFilePath(BaseFilesDir))
	if err != nil {
		return nil, err
	}

	return &cf, nil
}