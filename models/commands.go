package models

import (
	"database/sql"
	"errors"
	"strings"
)

type Command struct {
	Id int64 `json:"id"`
	ComponentId int64 `json:"componentId"`
	Name string `json:"name"`
	Value string `json:"value"`
	Result string `json:"result"`
}

type CommandsModel struct {
	DB *sql.DB
}

const CommandsFilterAllStmt = "SELECT id, component_id, name, value FROM commands WHERE component_id IN "
const CommandsInsertStmt = "INSERT INTO commands (component_id, name, value) VALUES (?, ?, ?);"
const CommandsUpdateStmt = "UPDATE commands SET name = ?, value = ? WHERE id = ?;"
const CommandsDeleteStmt = "DELETE FROM commands WHERE id = ?;"

func (model *CommandsModel) FilterAllCommands(componentIds []int64) ([]*Command, error) {

	if len(componentIds) == 0 {
		return nil, errors.New("componentIds must not be empty")
	}

	placeholders := make([]string, 0)
	cIds := make([]interface{}, 0)
	for i := 0; i < len(componentIds); i++ {
		placeholders = append(placeholders, "?")
		cIds = append(cIds, componentIds[i])
	}

	rows, err := model.DB.Query(CommandsFilterAllStmt + "(" + strings.Join(placeholders, ",") + ");", cIds...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	result := make([]*Command, 0)
	for rows.Next() {
		cmd := Command{}
		err := rows.Scan(&cmd.Id, &cmd.ComponentId, &cmd.Name, &cmd.Value)
		if err != nil {
			return nil, err
		}
		result = append(result, &cmd)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}

	return result, nil
}

func (model *CommandsModel) SaveCommand(cmd Command) (*Command, error) {

	var err error
	if cmd.Id != 0 {
		_, err = model.DB.Exec(CommandsUpdateStmt, cmd.Name, cmd.Value, cmd.Id)
	} else {
		res, err := model.DB.Exec(CommandsInsertStmt, cmd.ComponentId, cmd.Name, cmd.Value)
		if lastInsertId, err2 := res.LastInsertId(); err == nil && err2 == nil {
			cmd.Id = lastInsertId
		}
	}
	if err != nil {
		return &cmd, errors.New("500. Internal Server Error." + err.Error())
	}

	return &cmd, nil
}

func (model *CommandsModel) DeleteCommand(id int64) error {
	if id != 0 {
		_, err := model.DB.Exec(CommandsDeleteStmt, id)
		if err != nil {
			return errors.New("Error on delete operation")
		}
	}
	return nil
}