package config

import (
	"database/sql"
	"encoding/json"
	"github.com/roman-munteanu/config-finder/models"
	"golang.org/x/crypto/ssh"
	"html/template"
	"net/http"
)

type App struct {
	TPL *template.Template
	ComponentsModel interface {
		FilterAllComponents(teamId int64, envId int64) ([]*models.Component, error)
		FindByIdComponent(componentId int64) (*models.Component, error)
		SaveComponent(c models.Component) (*models.Component, error)
		DeleteComponent(id int64) error
	}
	TeamsModel interface {
		FindAllTeams() ([]*models.Team, error)
		SaveTeam(t models.Team) (*models.Team, error)
		DeleteTeam(id int64) error
	}
	EnvModel interface {
		FindAllEnvs() ([]*models.Environment, error)
		FilterAllEnvs(teamId int64) ([]*models.Environment, error)
		SaveEnv(env models.Environment) (*models.Environment, error)
		DeleteEnv(id int64) error
	}
	ConfigFileModel interface {
		FilterAllConfigFiles(componentId string) ([]*models.ConfigFile, error)
		SaveConfigFile(cf models.ConfigFile) (*models.ConfigFile, error)
		DeleteConfigFile(cf models.ConfigFile) (*models.ConfigFile, error)
	}
	CommandsModel interface {
		FilterAllCommands(componentIds []int64) ([]*models.Command, error)
		SaveCommand(cmd models.Command) (*models.Command, error)
		DeleteCommand(id int64) error
	}
	SshConfig *ssh.ClientConfig
}

func (app *App) RenderJSON(rw http.ResponseWriter, req *http.Request, data interface{}) {
	rw.Header().Set("Content-Type", "application/json")
	err := json.NewEncoder(rw).Encode(data)
	if err != nil {
		http.Error(rw, http.StatusText(500), http.StatusInternalServerError)
	}
}

func NewDB(driverName string, dataSourceName string) (*sql.DB, error) {
	db, err := sql.Open(driverName, dataSourceName)
	if err != nil {
		return nil, err
	}

	if err = db.Ping(); err != nil {
		return nil, err
	}

	return db, nil
}
