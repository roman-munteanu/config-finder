service-management-tool
-----

## Build
Install the app:
```
env CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go install main.go

chmod +x main
```

## Run

Run the app from command prompt:
```bash
./main
```

To shut down the app:
```
Ctrl + C
```

## Run in background
```bash
nohup ./main &
```

Kill the process:
```bash
ps -ef | grep main

kill -9 <p_id>
```


One can use the existing `main` without installing Go on your machine.

## Configuration

Find `app_config.yaml` file in the root project location. 



## Troubleshooting

In order to avoid this exception while connecting:
`ssh: handshake failed: ssh: unable to authenticate`
one needs to ssh to the corresponding box

Edit sshd_config:
```bash
vi /etc/ssh/sshd_config
a
```

Update from `no` to `yes`:
```bash
PasswordAuthentication yes
```

Save and exit: 
```bash
:wq
```

Restart sshd:
```bash
service sshd restart
```

Ensure that `PasswordAuthentication` has been updated:
```bash
cat /etc/ssh/sshd_config | grep PasswordAuthentication
```