package main

import (
	_ "github.com/go-sql-driver/mysql"
	"io/ioutil"
	//_ "github.com/mattn/go-sqlite3"
	"github.com/roman-munteanu/config-finder/config"
	"github.com/roman-munteanu/config-finder/handlers"
	"github.com/roman-munteanu/config-finder/models"
	"golang.org/x/crypto/ssh"
	"gopkg.in/yaml.v2"
	"html/template"
	"log"
	"net/http"
)

const ConfigFile = "app_config.yaml"

type AppConfig struct {
	DriverName string `yaml:"driverName"`
	DataSourceName string `yaml:"dataSourceName"`
	Port string `yaml:"port"`
	SshUsername string `yaml:"sshUsername"`
	SshPassword string `yaml:"sshPassword"`
}

var appConfig AppConfig
var app *config.App

func (ac *AppConfig) readConfig() *AppConfig {

	bs, err := ioutil.ReadFile(ConfigFile)
	if err != nil {
		log.Printf("Error reading config yaml file: #%v", err)
	}

	err = yaml.Unmarshal(bs, ac)
	if err != nil {
		log.Fatalf("Error while unmarshalling yaml config: %v", err)
	}

	return ac
}

func init() {

	appConfig = AppConfig{}
	appConfig.readConfig()

	db, err := config.NewDB(appConfig.DriverName, appConfig.DataSourceName)
	//db, err := config.NewDB("mysql", "root:password@tcp(localhost:3306)/servicemanagementtool")
	//db, err := config.NewDB("sqlite3", "C:\\Users\\romam\\software\\sqlite\\sqlite-tools-win32-x86-3300100\\smt.db")
	//db, err := config.NewDB("sqlite3", "/home/service-management-tool/smt.db")
	if err != nil {
		log.Panic(err)
	}

	app = &config.App {
		TPL: template.Must(template.ParseFiles("index.html")),
		ComponentsModel: &models.ComponentsModel{DB: db},
		TeamsModel: &models.TeamsModel{DB: db},
		EnvModel: &models.EnvModel{DB: db},
		ConfigFileModel: &models.ConfigFileModel{},
		CommandsModel: &models.CommandsModel{DB: db},
		SshConfig: &ssh.ClientConfig{
			User: appConfig.SshUsername,
			Auth: []ssh.AuthMethod{
				ssh.Password(appConfig.SshPassword),
			},
			HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		},
	}
}

func handleIndexSPA(app *config.App) http.Handler {
	return http.HandlerFunc(func (rw http.ResponseWriter, req * http.Request) {
		err := app.TPL.Execute(rw, nil)
		if err != nil {
			http.Error(rw, http.StatusText(500), http.StatusInternalServerError)
		}
	})
}

func main() {
	fileServer := http.FileServer(http.Dir("./static"))
	http.Handle("/static/", http.StripPrefix("/static", fileServer))

	http.Handle("/favicon.ico", http.NotFoundHandler())
	http.Handle("/", handleIndexSPA(app))

	http.Handle("/components", handlers.FilterAllComponents(app))
	http.Handle("/components/save", handlers.SaveComponent(app))
	http.Handle("/components/delete", handlers.DeleteComponent(app))

	http.Handle("/teams", handlers.FindAllTeams(app))
	http.Handle("/teams/save", handlers.SaveTeam(app))
	http.Handle("/teams/delete", handlers.DeleteTeam(app))

	http.Handle("/environments", handlers.FindAllEnvs(app))
	http.Handle("/environments/filter", handlers.FilterAllEnvs(app))
	http.Handle("/environments/save", handlers.SaveEnv(app))
	http.Handle("/environments/delete", handlers.DeleteEnv(app))

	http.Handle("/configfiles/list", handlers.FilterAllConfigFiles(app))
	http.Handle("/configfiles/save", handlers.SaveConfigFile(app))
	http.Handle("/configfiles/delete", handlers.DeleteConfigFile(app))

	http.Handle("/commands", handlers.FilterAllCommands(app))
	http.Handle("/commands/save", handlers.SaveCommand(app))
	http.Handle("/commands/delete", handlers.DeleteCommand(app))
	http.Handle("/commands/execute", handlers.ExecuteCommand(app))

	http.Handle("/health/check", handlers.HealthCheck(app))

	log.Fatal(http.ListenAndServe(":" + appConfig.Port, nil))
}
