package main

import (
	"bytes"
	"fmt"
	"golang.org/x/crypto/ssh"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
	"text/template"
)

//const serviceConfigFilePath = "service_config.yaml"

const cmdSep = "echo -------"

var sc ServiceConfig
var sshConfig *ssh.ClientConfig
var tpl *template.Template

type AppCredentials struct {
	Username string `yaml:"username" json:"username"`
	Password string `yaml:"password" json:"password"`
}

type AppCommand struct {
	Command string `yaml:"command" json:"command"`
	Value string `yaml:"value" json:"value"`
}

type AppService struct {
	Name string `yaml:"name" json:"name"`
	Protocol string `yaml:"protocol" json:"protocol"`
	Host string `yaml:"host" json:"host"`
	Port int64 `yaml:"port" json:"port"`
	StatusEndpoint string `yaml:"statusEndpoint" json:"statusEndpoint"`
	Status string `yaml:"status" json:"status"`
	Commands []AppCommand `yaml:"commands" json:"commands"`
}

type ServiceConfig struct {
	AppCredentials `yaml:"credentials" json:"credentials"`
	Services []AppService `yaml:"services" json:"services"`
}

func (sc *ServiceConfig) getServiceConfig(configLocation string) *ServiceConfig {

	bs, err := ioutil.ReadFile(configLocation)
	if err != nil {
		log.Printf("Error reading yaml config file: #%v", err)
	}

	err = yaml.Unmarshal(bs, sc)
	if err != nil {
		log.Fatalf("Error while unmarshalling yaml: %v", err)
	}

	return sc
}

func logError(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

func handleService(serviceIndex int, service AppService) {

	if len(service.StatusEndpoint) > 0 {
		response,err := http.Get(service.Protocol + "://" + service.Host + ":" +  strconv.Itoa(int(service.Port)) + service.StatusEndpoint)
		if err != nil {
			//log.Fatalf("Error sending request: %v", err)
			sc.Services[serviceIndex].Status = err.Error()
		} else {
			data,err := ioutil.ReadAll(response.Body)
			if err != nil {
				log.Fatalf("Error reading response: %v", err)
			}
			sc.Services[serviceIndex].Status = string(data)
		}
	}

	// single command
	/**
	if len(service.Commands) > 0 {
		conn, err := ssh.Dial("tcp", service.Host + ":22", sshConfig)
		if err != nil {
			log.Fatalln(err)
		}
		defer conn.Close()

		session,err := conn.NewSession()
		if err != nil {
			log.Fatalln(err)
		}
		defer session.Close()

		var b bytes.Buffer
		session.Stdout = &b

		if err = session.Run(service.Commands[0].Command); err != nil {
			log.Fatalln(err)
		}
		sc.Services[serviceIndex].Commands[0].Value = b.String()
		b.Reset()
	}
	/**/

	// multiple commands
	if len(service.Commands) > 0 {
		conn, err := ssh.Dial("tcp", service.Host + ":22", sshConfig)
		logError(err)
		defer conn.Close()

		session,err := conn.NewSession()
		logError(err)
		defer session.Close()

		stdin, err := session.StdinPipe()
		logError(err)

		var stdoutBuf bytes.Buffer
		session.Stdout = &stdoutBuf

		err = session.Shell()
		logError(err)

		for _, command := range service.Commands {
			_, err := fmt.Fprintf(stdin, "%s\n%s\n", command.Command, cmdSep)
			logError(err)
		}
		_, err = stdin.Write([]byte("exit\n"))
		logError(err)

		err = session.Wait()
		logError(err)

		results := strings.Split(stdoutBuf.String(), "-------")

		if len(results) > 0 {
			for resIdx, res := range results[:len(results)-1] {
				sc.Services[serviceIndex].Commands[resIdx].Value = res
			}
		}

		stdoutBuf.Reset()
	}
}

func init() {
	tpl = template.Must(template.ParseFiles("index-old.gohtml"))
}

func handleIndex(rw http.ResponseWriter, req *http.Request) {

	configParam := req.FormValue("config")

	sc.getServiceConfig(configParam)
	fmt.Println(sc)

	sshConfig = &ssh.ClientConfig{
		User: sc.AppCredentials.Username,
		Auth: []ssh.AuthMethod{
			ssh.Password(sc.AppCredentials.Password),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}

	for sIdx, service := range sc.Services {
		handleService(sIdx, service)
	}

	//bs, err := json.Marshal(sc)
	//if err != nil {
	//	log.Fatalf("Error marshalling to json: %v", err)
	//}
	//_, err = io.WriteString(rw, string(bs))
	//if err != nil {
	//	log.Fatalf("Error writing string: %v", err)
	//}

	err := tpl.ExecuteTemplate(rw, "index-old.gohtml", sc)
	if err != nil {
		log.Fatalf("Error executing template: %v", err)
	}
}

//func main() {
//	http.HandleFunc("/", handleIndex)
//	http.Handle("/favicon.ico", http.NotFoundHandler())
//	log.Fatal(http.ListenAndServe(":8989", nil))
//}
