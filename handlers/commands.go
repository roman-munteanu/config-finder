package handlers

import (
	"bytes"
	"encoding/json"
	"github.com/roman-munteanu/config-finder/config"
	"github.com/roman-munteanu/config-finder/models"
	"golang.org/x/crypto/ssh"
	"net/http"
	"strconv"
)

func FilterAllCommands(app *config.App) http.Handler {
	return http.HandlerFunc(func (rw http.ResponseWriter, req * http.Request) {

		if req.Method != "GET" {
			http.Error(rw, http.StatusText(405), http.StatusMethodNotAllowed)
			return
		}

		componentIdValue := req.FormValue("componentId")
		if componentIdValue == "" {
			http.Error(rw, "Bad Request: componentId is required", http.StatusBadRequest)
			return
		}

		componentId, err := strconv.ParseInt(componentIdValue, 0, 64)
		if err != nil {
			http.Error(rw, "teamId must be a number.", http.StatusBadRequest)
			return
		}

		result, err := app.CommandsModel.FilterAllCommands([]int64{componentId})
		if err != nil {
			http.Error(rw, http.StatusText(500), http.StatusInternalServerError)
			return
		}

		app.RenderJSON(rw, req, result)
	})
}


func SaveCommand(app *config.App) http.Handler {
	return http.HandlerFunc(func (rw http.ResponseWriter, req * http.Request) {

		if req.Method != "POST" {
			http.Error(rw, http.StatusText(405), http.StatusMethodNotAllowed)
			return
		}

		cmd := models.Command{}

		err := json.NewDecoder(req.Body).Decode(&cmd)
		if err != nil {
			http.Error(rw, "Error decoding JSON body", http.StatusBadRequest)
			return
		}

		result,err := app.CommandsModel.SaveCommand(cmd)
		if err != nil {
			http.Error(rw, http.StatusText(400), http.StatusBadRequest)
			return
		}

		app.RenderJSON(rw, req, result)
	})
}


func DeleteCommand(app *config.App) http.Handler {
	return http.HandlerFunc(func (rw http.ResponseWriter, req * http.Request) {

		if req.Method != "DELETE" {
			http.Error(rw, http.StatusText(405), http.StatusMethodNotAllowed)
			return
		}

		var id int64 = 0
		if idNum, err := strconv.ParseInt(req.FormValue("id"), 0, 64); err == nil {
			id = idNum
		}
		if id == 0 {
			http.Error(rw, http.StatusText(400), http.StatusBadRequest)
			return
		}

		err := app.CommandsModel.DeleteCommand(id)
		if err != nil {
			http.Error(rw, http.StatusText(400), http.StatusBadRequest)
			return
		}

		app.RenderJSON(rw, req, id)
	})
}

func ExecuteCommand(app *config.App) http.Handler {
	return http.HandlerFunc(func (rw http.ResponseWriter, req * http.Request) {

		if req.Method != "POST" {
			http.Error(rw, http.StatusText(405), http.StatusMethodNotAllowed)
			return
		}

		cmd := models.Command{}

		err := json.NewDecoder(req.Body).Decode(&cmd)
		if err != nil {
			http.Error(rw, "Error decoding JSON body", http.StatusBadRequest)
			return
		}

		component, err := app.ComponentsModel.FindByIdComponent(cmd.ComponentId)
		if err != nil {
			http.Error(rw, "Error extracting component", http.StatusInternalServerError)
			return
		}

		conn, err := ssh.Dial("tcp", component.IpAddress + ":22", app.SshConfig)
		if err != nil {
			http.Error(rw, "Error establishing connection", http.StatusInternalServerError)
			return
		}
		defer conn.Close()

		session,err := conn.NewSession()
		if err != nil {
			http.Error(rw, "Error opening session", http.StatusInternalServerError)
			return
		}
		defer session.Close()

		var b bytes.Buffer
		session.Stdout = &b

		if err = session.Run(cmd.Value); err != nil {
			http.Error(rw, "Error running command", http.StatusInternalServerError)
			return
		}
		cmd.Result = b.String()
		b.Reset()

		app.RenderJSON(rw, req, cmd)
	})
}