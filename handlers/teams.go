package handlers

import (
	"encoding/json"
	"github.com/roman-munteanu/config-finder/config"
	"github.com/roman-munteanu/config-finder/models"
	"net/http"
	"strconv"
)

func FindAllTeams(app *config.App) http.Handler {
	return http.HandlerFunc(func (rw http.ResponseWriter, req * http.Request) {

		if req.Method != "GET" {
			http.Error(rw, http.StatusText(405), http.StatusMethodNotAllowed)
			return
		}

		result, err := app.TeamsModel.FindAllTeams()
		if err != nil {
			http.Error(rw, http.StatusText(500), http.StatusInternalServerError)
			return
		}

		app.RenderJSON(rw, req, result)
	})
}

func SaveTeam(app *config.App) http.Handler {
	return http.HandlerFunc(func (rw http.ResponseWriter, req * http.Request) {

		if req.Method != "POST" {
			http.Error(rw, http.StatusText(405), http.StatusMethodNotAllowed)
			return
		}

		t := models.Team{}

		err := json.NewDecoder(req.Body).Decode(&t)
		if err != nil {
			http.Error(rw, "Error decoding JSON body", http.StatusBadRequest)
			return
		}

		result,err := app.TeamsModel.SaveTeam(t)
		if err != nil {
			http.Error(rw, http.StatusText(400), http.StatusBadRequest)
			return
		}

		app.RenderJSON(rw, req, result)
	})
}


func DeleteTeam(app *config.App) http.Handler {
	return http.HandlerFunc(func (rw http.ResponseWriter, req * http.Request) {

		if req.Method != "DELETE" {
			http.Error(rw, http.StatusText(405), http.StatusMethodNotAllowed)
			return
		}

		var id int64 = 0
		if idNum, err := strconv.ParseInt(req.FormValue("id"), 0, 64); err == nil {
			id = idNum
		}
		if id == 0 {
			http.Error(rw, http.StatusText(400), http.StatusBadRequest)
			return
		}

		err := app.TeamsModel.DeleteTeam(id)
		if err != nil {
			http.Error(rw, http.StatusText(400), http.StatusBadRequest)
			return
		}

		app.RenderJSON(rw, req, id)
	})
}