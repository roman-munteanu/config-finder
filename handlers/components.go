package handlers

import (
	"encoding/json"
	"github.com/roman-munteanu/config-finder/config"
	"github.com/roman-munteanu/config-finder/models"
	"net/http"
	"strconv"
)

func FilterAllComponents(app *config.App) http.Handler {
	return http.HandlerFunc(func (rw http.ResponseWriter, req * http.Request) {

		if req.Method != "GET" {
			http.Error(rw, http.StatusText(405), http.StatusMethodNotAllowed)
			return
		}

		teamIdValue := req.FormValue("teamId")
		if teamIdValue == "" {
			http.Error(rw, "Bad Request: teamId is required", http.StatusBadRequest)
			return
		}

		envIdValue := req.FormValue("envId")
		if envIdValue == "" {
			http.Error(rw, "Bad Request: envId is required", http.StatusBadRequest)
			return
		}

		teamId, err := strconv.ParseInt(teamIdValue, 0, 32)
		if err != nil {
			http.Error(rw, "teamId must be a number.", http.StatusBadRequest)
			return
		}

		envId, err := strconv.ParseInt(envIdValue, 0, 32)
		if err != nil {
			http.Error(rw, "envId must be a number.", http.StatusBadRequest)
			return
		}

		result, err := app.ComponentsModel.FilterAllComponents(teamId, envId)
		if err != nil {
			http.Error(rw, "Error filtering components", http.StatusInternalServerError)
			return
		}

		if len(result) > 0 {
			cIds := make([]int64, 0)
			for _, component := range result {
				cIds = append(cIds, component.Id)
			}

			commands, err := app.CommandsModel.FilterAllCommands(cIds)
			if err != nil {
				http.Error(rw, "Error extracting commands", http.StatusInternalServerError)
				return
			}

			if len(commands) > 0 {
				grouped := make(map[int64][]*models.Command)
				for _, cmd := range commands {
					if _, ok := grouped[cmd.ComponentId]; !ok {
						grouped[cmd.ComponentId] = make([]*models.Command, 0)
					}
					grouped[cmd.ComponentId] = append(grouped[cmd.ComponentId], cmd)
				}

				for _, cpnt := range result {
					if _, ok := grouped[cpnt.Id]; ok {
						cpnt.Commands = grouped[cpnt.Id]
					}
				}
			}
		}

		app.RenderJSON(rw, req, result)
	})
}

func SaveComponent(app *config.App) http.Handler {
	return http.HandlerFunc(func (rw http.ResponseWriter, req * http.Request) {

		if req.Method != "POST" {
			http.Error(rw, http.StatusText(405), http.StatusMethodNotAllowed)
			return
		}

		c := models.Component{}

		err := json.NewDecoder(req.Body).Decode(&c)
		if err != nil {
			http.Error(rw, "Error decoding JSON body", http.StatusBadRequest)
			return
		}

		if !c.IsValid() {
			http.Error(rw, c.GetErrorsStr(), http.StatusBadRequest)
			return
		}

		result,err := app.ComponentsModel.SaveComponent(c)
		if err != nil {
			http.Error(rw, http.StatusText(400), http.StatusBadRequest)
			return
		}

		app.RenderJSON(rw, req, result)
	})
}

func DeleteComponent(app *config.App) http.Handler {
	return http.HandlerFunc(func (rw http.ResponseWriter, req * http.Request) {

		if req.Method != "DELETE" {
			http.Error(rw, http.StatusText(405), http.StatusMethodNotAllowed)
			return
		}

		var cId int64 = 0
		if idNum, err := strconv.ParseInt(req.FormValue("id"), 0, 64); err == nil {
			cId = idNum
		}
		if cId == 0 {
			http.Error(rw, http.StatusText(400), http.StatusBadRequest)
			return
		}

		err := app.ComponentsModel.DeleteComponent(cId)
		if err != nil {
			http.Error(rw, http.StatusText(400), http.StatusBadRequest)
			return
		}

		app.RenderJSON(rw, req, cId)
	})
}