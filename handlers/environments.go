package handlers

import (
	"encoding/json"
	"github.com/roman-munteanu/config-finder/config"
	"github.com/roman-munteanu/config-finder/models"
	"net/http"
	"strconv"
)

func FindAllEnvs(app *config.App) http.Handler {
	return http.HandlerFunc(func (rw http.ResponseWriter, req * http.Request) {

		if req.Method != "GET" {
			http.Error(rw, http.StatusText(405), http.StatusMethodNotAllowed)
			return
		}

		result, err := app.EnvModel.FindAllEnvs()
		if err != nil {
			http.Error(rw, http.StatusText(500), http.StatusInternalServerError)
			return
		}

		app.RenderJSON(rw, req, result)
	})
}

func FilterAllEnvs(app *config.App) http.Handler {
	return http.HandlerFunc(func (rw http.ResponseWriter, req * http.Request) {

		if req.Method != "GET" {
			http.Error(rw, http.StatusText(405), http.StatusMethodNotAllowed)
			return
		}

		teamIdValue := req.FormValue("teamId")
		if teamIdValue == "" {
			http.Error(rw, "Bad Request: teamId is required", http.StatusBadRequest)
			return
		}

		teamId, err := strconv.ParseInt(teamIdValue, 0, 32)
		if err != nil {
			http.Error(rw, "teamId must be a number.", http.StatusBadRequest)
			return
		}

		result, err := app.EnvModel.FilterAllEnvs(teamId)
		if err != nil {
			http.Error(rw, http.StatusText(500), http.StatusInternalServerError)
			return
		}

		app.RenderJSON(rw, req, result)
	})
}

func SaveEnv(app *config.App) http.Handler {
	return http.HandlerFunc(func (rw http.ResponseWriter, req * http.Request) {

		if req.Method != "POST" {
			http.Error(rw, http.StatusText(405), http.StatusMethodNotAllowed)
			return
		}

		env := models.Environment{}

		err := json.NewDecoder(req.Body).Decode(&env)
		if err != nil {
			http.Error(rw, "Error decoding JSON body", http.StatusBadRequest)
			return
		}

		result,err := app.EnvModel.SaveEnv(env)
		if err != nil {
			http.Error(rw, http.StatusText(400), http.StatusBadRequest)
			return
		}

		app.RenderJSON(rw, req, result)
	})
}


func DeleteEnv(app *config.App) http.Handler {
	return http.HandlerFunc(func (rw http.ResponseWriter, req * http.Request) {

		if req.Method != "DELETE" {
			http.Error(rw, http.StatusText(405), http.StatusMethodNotAllowed)
			return
		}

		var id int64 = 0
		if idNum, err := strconv.ParseInt(req.FormValue("id"), 0, 64); err == nil {
			id = idNum
		}
		if id == 0 {
			http.Error(rw, http.StatusText(400), http.StatusBadRequest)
			return
		}

		err := app.EnvModel.DeleteEnv(id)
		if err != nil {
			http.Error(rw, http.StatusText(400), http.StatusBadRequest)
			return
		}

		app.RenderJSON(rw, req, id)
	})
}