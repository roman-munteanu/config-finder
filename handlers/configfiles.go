package handlers

import (
	"github.com/roman-munteanu/config-finder/config"
	"github.com/roman-munteanu/config-finder/models"
	"io/ioutil"
	"net/http"
)

func FilterAllConfigFiles(app *config.App) http.Handler {
	return http.HandlerFunc(func (rw http.ResponseWriter, req * http.Request) {

		if req.Method != "GET" {
			http.Error(rw, http.StatusText(405), http.StatusMethodNotAllowed)
			return
		}

		componentId := req.FormValue("componentId")
		if componentId == "" {
			http.Error(rw, "Bad Request: componentId is required", http.StatusBadRequest)
			return
		}

		result, err := app.ConfigFileModel.FilterAllConfigFiles(componentId)
		if err != nil {
			http.Error(rw, http.StatusText(500), http.StatusInternalServerError)
			return
		}

		app.RenderJSON(rw, req, result)
	})
}

func SaveConfigFile(app *config.App) http.Handler {
	return http.HandlerFunc(func (rw http.ResponseWriter, req * http.Request) {

		if req.Method != "POST" {
			http.Error(rw, http.StatusText(405), http.StatusMethodNotAllowed)
			return
		}

		componentId := req.FormValue("componentId")
		if componentId == "" {
			http.Error(rw, "Bad Request: componentId is required", http.StatusBadRequest)
			return
		}

		file, header, err := req.FormFile("configfile")
		if err != nil {
			http.Error(rw, "Error processing file", http.StatusInternalServerError)
			return
		}
		defer file.Close()

		bs, err := ioutil.ReadAll(file)
		if err != nil {
			http.Error(rw, "Error reading file", http.StatusInternalServerError)
			return
		}

		cf := models.ConfigFile{
			ComponentId: componentId,
			Name: header.Filename,
			Body: bs,
		}

		result,err := app.ConfigFileModel.SaveConfigFile(cf)
		if err != nil {
			http.Error(rw, http.StatusText(400), http.StatusBadRequest)
			return
		}

		app.RenderJSON(rw, req, result)
	})
}

func DeleteConfigFile(app *config.App) http.Handler {
	return http.HandlerFunc(func (rw http.ResponseWriter, req * http.Request) {

		if req.Method != "DELETE" {
			http.Error(rw, http.StatusText(405), http.StatusMethodNotAllowed)
			return
		}

		componentId := req.FormValue("componentId")
		if componentId == "" {
			http.Error(rw, "Bad Request: componentId is required", http.StatusBadRequest)
			return
		}

		fileName := req.FormValue("fileName")
		if fileName == "" {
			http.Error(rw, "Bad Request: fileName is required", http.StatusBadRequest)
			return
		}

		cf := models.ConfigFile{
			ComponentId: componentId,
			Name: fileName,
		}

		result, err := app.ConfigFileModel.DeleteConfigFile(cf)
		if err != nil {
			http.Error(rw, http.StatusText(400), http.StatusBadRequest)
			return
		}

		app.RenderJSON(rw, req, result)
	})
}