package handlers

import (
	"github.com/roman-munteanu/config-finder/config"
	"github.com/roman-munteanu/config-finder/models"
	"io/ioutil"
	"net/http"
)

func HealthCheck(app *config.App) http.Handler {
	return http.HandlerFunc(func (rw http.ResponseWriter, req * http.Request) {

		if req.Method != "GET" {
			http.Error(rw, http.StatusText(405), http.StatusMethodNotAllowed)
			return
		}

		endpoint := req.FormValue("endpoint")
		if endpoint == "" {
			http.Error(rw, "Bad Request: endpoint is required", http.StatusBadRequest)
			return
		}

		hc := models.HealthCheck{}

		resp, err := http.Get(endpoint)
		if err != nil {
			hc.Error = err.Error()
			app.RenderJSON(rw, req, hc)
			return
		}
		defer resp.Body.Close()

		hc.Status = resp.Status
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			hc.Error = err.Error()
			app.RenderJSON(rw, req, hc)
			return
		}
		hc.Body = string(body)

		app.RenderJSON(rw, req, hc)
	})
}
