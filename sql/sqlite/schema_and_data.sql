
-- sqlite3 smt.db

DROP TABLE teams;

CREATE TABLE teams (
    id   INTEGER PRIMARY KEY NOT NULL,
    name VARCHAR NOT NULL
);

INSERT INTO teams (name) VALUES ('Team A'), ('Team B'), ('Team C');
SELECT * FROM teams;




DROP TABLE environments;

CREATE TABLE environments (
    id   INTEGER PRIMARY KEY NOT NULL,
    team_id INT NOT NULL,
    name VARCHAR(100) NOT NULL,
    FOREIGN KEY (team_id) REFERENCES teams(id) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO environments (team_id, name) VALUES (1, 'DEV'), (1, 'QA'), (1, 'UAT'), (1, 'PROD'), (2, 'DEV'), (2, 'QA');
SELECT * FROM environments;



DROP TABLE components;

CREATE TABLE components (
    id   INTEGER PRIMARY KEY NOT NULL,
    team_id INTEGER NOT NULL,
    env_id INTEGER NOT NULL,
    name VARCHAR NOT NULL,
    protocol VARCHAR DEFAULT NULL,
    port SMALLINT DEFAULT 0,
    ip_address VARCHAR DEFAULT NULL,
    domain_name VARCHAR DEFAULT NULL,
    status_path VARCHAR DEFAULT NULL,
    health_path VARCHAR DEFAULT NULL,
    description TEXT DEFAULT NULL,
    FOREIGN KEY (team_id) REFERENCES teams(id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (env_id) REFERENCES environments(id) ON DELETE CASCADE ON UPDATE CASCADE
);


INSERT INTO components (team_id, env_id, name, protocol, port, ip_address, domain_name, status_path, health_path, description) VALUES
(1, 1, 'Project A', 'http', 8080, '127.0.0.1', 'a.domain', '/status', '/actuator/health', 'Description A'),
(1, 1, 'Project B', 'https', 8443, '192.168.0.1', 'b.domain', '/b/status', '/b/health', 'Description B');

SELECT * FROM components;



DROP TABLE commands;

CREATE TABLE commands (
    id   INTEGER PRIMARY KEY NOT NULL,
    component_id INTEGER NOT NULL,
    name VARCHAR DEFAULT NULL,
    value VARCHAR NOT NULL,
    FOREIGN KEY (component_id) REFERENCES components(id) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO commands (component_id, name, value) VALUES (1, 'Check current user', 'whoami'), (1, 'Show configuration', 'cat /etc/opt/application.properties');
SELECT * FROM commands;
