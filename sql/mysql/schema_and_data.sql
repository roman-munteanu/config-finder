
CREATE DATABASE servicemanagementtool;

USE servicemanagementtool;


-- ################## TEAMS
DROP TABLE teams;

CREATE TABLE teams (
    id INT(11) NOT NULL AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO teams (name) VALUES ('Team A'), ('Team B'), ('Team C');
SELECT * FROM teams;


-- ################## ENVIRONMENTS
DROP TABLE environments;

CREATE TABLE environments (
    id INT(11) NOT NULL AUTO_INCREMENT,
    team_id INT NOT NULL,
    name VARCHAR(100) NOT NULL,
    KEY idx_environments_team_id (team_id),
    CONSTRAINT FK_environments_teams FOREIGN KEY (team_id) REFERENCES teams(id) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (id)
);

INSERT INTO environments (team_id, name) VALUES (1, 'DEV'), (1, 'QA'), (1, 'UAT'), (1, 'PROD'), (2, 'DEV'), (2, 'QA');
SELECT * FROM environments;


-- ################## COMPONENTS
DROP TABLE components;

CREATE TABLE components (
  id INT(11) NOT NULL AUTO_INCREMENT,
  team_id INT(11) NOT NULL,
  env_id INT(11) NOT NULL,
  name VARCHAR(255) NOT NULL,
  protocol VARCHAR(10) DEFAULT NULL,
  port SMALLINT DEFAULT 0,
  ip_address VARCHAR(100) DEFAULT NULL,
  domain_name VARCHAR(255) DEFAULT NULL,
  status_path VARCHAR(255) DEFAULT NULL,
  health_path VARCHAR(255) DEFAULT NULL,
  description TEXT DEFAULT NULL,
  KEY idx_components_team_id_env_id (team_id, env_id),
  CONSTRAINT FK_components_teams FOREIGN KEY (team_id) REFERENCES teams(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_components_environments FOREIGN KEY (env_id) REFERENCES environments(id) ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO components (team_id, env_id, name, protocol, port, ip_address, domain_name, status_path, health_path, description) VALUES
(1, 1, 'Project A', 'http', 8080, '127.0.0.1', 'a.domain', '/status', '/actuator/health', 'Description A'),
(1, 1, 'Project B', 'https', 8443, '192.168.0.1', 'b.domain', '/b/status', '/b/health', 'Description B');

SELECT * FROM components;


-- ################## COMMANDS
DROP TABLE commands;

CREATE TABLE commands (
    id INT(11) NOT NULL AUTO_INCREMENT,
    component_id INT(11) NOT NULL,
    name VARCHAR(100) DEFAULT NULL,
    value VARCHAR(255) NOT NULL,
    KEY idx_commands_component_id (component_id),
    CONSTRAINT FK_commands_components FOREIGN KEY (component_id) REFERENCES components(id) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO commands (component_id, name, value) VALUES (1, 'Check current user', 'whoami'), (1, 'Show configuration', 'cat /etc/opt/application.properties');
SELECT * FROM commands;