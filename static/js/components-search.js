Vue.component('components-search-box', {
    data: function() {
        return {
          teams: [],
          environments: [],
          selectedTeam: {
            environments: []
          },
          selectedEnv: {}
        };
    },
  created: function() {

  },
  methods: {
      updateTeamsList: function(teamsList) {
          this.teams = teamsList;
          this.prepareFilters();
      },
      updateEnvList: function(envList) {
          this.environments = envList;
          this.prepareFilters();
      },
      prepareFilters: function() {
          var teamsData = this.teams;
          var envData = this.environments;

          if (teamsData && teamsData.length && envData && envData.length) {

              var envMap = {};
              for (var i=0; i<envData.length; i++) {
                  var env = envData[i];
                  if (envMap.hasOwnProperty(env.teamId)) {
                      envMap[env.teamId].push(env);
                  } else {
                      envMap[env.teamId] = [env];
                  }
              }
              for (var i=0; i<teamsData.length; i++) {
                  var teamId = teamsData[i].id;
                  this.teams[i]['environments'] = envMap.hasOwnProperty(teamId) ? envMap[teamId] : [];
              }
          }
      },
    broadcastSearchComponentsEvent: function(event) {
      event.preventDefault();
      var filterObj = {
        teamId: this.selectedTeam.id,
        envId: this.selectedEnv.id
      };
      this.$emit('components-search-event', filterObj);
      console.log('filterObj:');
      console.log(filterObj);
    }
  },
  computed: {
    setSelectedTeam: {
      get: function() {
        return this.selectedTeam.id;
      },
      set: function(optionValue) {
        var teamsResult = this.teams.filter(t => t.id == optionValue);
        this.selectedTeam = teamsResult.length ? teamsResult[0] : {environments: []};
      }
    },
    setSelectedEnv: {
      get: function() {
        return this.selectedEnv.id;
      },
      set: function(optionValue) {
        var envsResult = this.environments.filter(env => env.id == optionValue);
        this.selectedEnv = envsResult.length ? envsResult[0] : {};
      }
    }
  },
  template: `
    <div class="row mb-4">
      <form class="form-inline" v-on:submit="broadcastSearchComponentsEvent">
          <label class="my-1 mr-2" for="inlineFormTeam">Team:</label>
          <select name="teamId" id="inlineFormTeam" class="form-control my-1 mr-sm-2 mr-3" v-model="setSelectedTeam">
              <option v-for="team in teams" :key="team.id" :value="team.id">{{ team.name }}</option>
          </select>
      
          <label class="my-1 mr-2" for="inlineFormEnv">Environment:</label>
          <select name="envId" id="inlineFormEnv" class="form-control mr-3" v-model="setSelectedEnv">
              <option v-for="env in selectedTeam.environments" :key="env.id" :value="env.id">{{ env.name }}</option>
          </select>
      
          <button type="submit" class="btn btn-primary my-1">Search</button>
      </form>
    </div> 
  `
});