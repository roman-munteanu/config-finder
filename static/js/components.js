Vue.component('components-box', {
    data: function() {
        return {
            items: [],
            selectedItem: {},
            isLoading: false,
            mode: "view",
            filterObj: {}
        };
    },
    componentsStore: {},
    editor: null,
    created: function() {
        // this.componentsStore = store.newStore('local', initialData['components']);
        var storeData = {
            findAllUrl: '/components',
            findComponentsUrl: '/components',
            saveUrl: '/components/save',
            deleteUrl: '/components/delete'
        };
        this.componentsStore = store.newStore('server', null, storeData);
    },
    methods: {
        switchMode: function(mode) {
            var self = this;

            this.mode = mode;
            if (mode == 'add') {
                this.selectedItem = {};
            }
            if (mode == 'add' || mode == 'edit') {

                if (this.editor) {
                    this.editor.removeInstance('compDescription');
                    this.editor = null;
                }
                jQuery(document).ready(function() {
                    self.editor = new nicEditor().panelInstance('compDescription');
                });
            }
        },
        filterItems: function(filterObj) {
            var self = this;
            this.filterObj = filterObj;
            this.componentsStore.findComponents(this.filterObj).then(function(data) {
                self.items = data;
                self.appendAdditionalData();
            });
        },
        showItem: function(it) {
            this.selectedItem = it;
        },
        saveItem: function(event) {
            event.preventDefault();

            this.selectedItem.description = nicEditors.findEditor('compDescription').getContent();

            // TODO validation
            var it = this.selectedItem;
            var item = {
                name: it.name,
                protocol: it.protocol,
                ipAddress: it.ipAddress,
                port: it.port ? parseInt(it.port, 10) : 0,
                domainName: it.domainName,
                statusPath: it.statusPath,
                healthPath: it.healthPath,
                description: it.description
            };
            if (it.id) item.id = it.id;
            item.teamId = this.filterObj.teamId;
            item.envId = this.filterObj.envId;

            this.componentsStore.save(item);
        },
        deleteItem: function(it) {
            this.componentsStore.delete(it);
        },
        appendAdditionalData: function() {
            var items = this.items;
            if (items && items.length) {
                for (var i=0; i<items.length; i++) {
                    var item = items[i];

                    item.endpoints = [];
                    var protocol = item.protocol ? item.protocol : "http";
                    var hosts = [];

                    if (item.ipAddress) {
                        var host = protocol + '://' + item.ipAddress;
                        if (item.port) {
                            host += ':' + item.port;
                        }
                        hosts[hosts.length] = host;
                    }

                    if (item.domainName) {
                        var host = protocol + '://' + item.domainName;
                        if (item.port) {
                            host += ':' + item.port;
                        }
                        hosts[hosts.length] = host;
                    }

                    if (hosts.length) {
                        for (var j=0; j<hosts.length; j++) {
                            if (item.statusPath) {
                                item.endpoints[item.endpoints.length] = hosts[j] + item.statusPath;
                            }
                            if (item.healthPath) {
                                item.endpoints[item.endpoints.length] = hosts[j] + item.healthPath;
                            }
                        }
                    }

                }
            }
        }
    },
    computed: {

    },
    template: `
    <div class="row mb-4">
        <div class="col-4">
            <p v-show="isLoading"><strong>Loading ...</strong></p>
            <ul class="list-group">
                <li class="list-group-item" v-for="item in items" :key="item.id" v-bind:class="{ active: item.id == selectedItem.id }" v-on:click="showItem(item)">
                    {{ item.name }}
                </li>
            </ul>
        </div> 

        <div class="col-8">
            <div class="btn-group btn-group-sm mb-2" role="group">
                <button type="button" class="btn btn-secondary" v-bind:class="{ active: mode == 'view' }" v-on:click="switchMode('view')">View</button>
                <button type="button" class="btn btn-secondary" v-bind:class="{ active: mode == 'edit' }" v-on:click="switchMode('edit')">Edit</button>
                <button type="button" class="btn btn-secondary" v-bind:class="{ active: mode == 'add' }" v-on:click="switchMode('add')">Add</button>
            </div>
            
            <div class="card mb-3" id="compViewMode" v-if="mode == 'view'">
                <h4 class="card-header text-white bg-info">{{ selectedItem.name }}</h4>
                <div class="card-body">
                    <ul>
                        <li v-show="selectedItem.protocol">Protocol: <b>{{ selectedItem.protocol }}</b></li>
                        <li v-show="selectedItem.ipAddress">IP: <b>{{ selectedItem.ipAddress }}</b></li>
                        <li v-show="selectedItem.domainName">Domain: <b>{{ selectedItem.domainName }}</b></li>
                        <li v-show="selectedItem.port">Port: <b>{{ selectedItem.port }}</b></li>
                    </ul>   
                    
                    <hr />
                    <h5>Description</h5>
                    <div v-html="selectedItem.description">{{ selectedItem.description }}</div>
                    
                </div>
            </div>
            
            <div class="card mb-3" id="healthCheckView" v-show="mode == 'view' && selectedItem.endpoints && selectedItem.endpoints.length">
                <div class="card-header">Health-Check</div>
                <div class="card-body">
                    <health-check-box v-for="endpoint in selectedItem.endpoints" v-bind:endpoint="endpoint"></health-check-box>
                </div>
            </div>
            
            <div class="card" id="compEditMode" v-if="mode == 'edit' || mode == 'add'">
                <form class="card-body" v-on:submit="saveItem">
                    <input type="hidden" name="id" value="selectedItem.id" />
                    <div class="form-group">
                        <label for="compName">Name:</label>
                        <input type="text" class="form-control" name="name" id="compName" v-model="selectedItem.name" />
                    </div>
                    <div class="form-group">
                        <label for="compProtocol">Protocol:</label>
                        <input type="text" class="form-control" name="protocol" id="compProtocol" v-model="selectedItem.protocol" />
                    </div>
                    <div class="form-group">
                        <label for="compIP">IP Address:</label>
                        <input type="text" class="form-control" name="ipAddress" id="compIP" v-model="selectedItem.ipAddress" />
                    </div>
                    <div class="form-group">
                        <label for="compPort">Port:</label>
                        <input type="text" class="form-control" name="port" id="compPort" v-model="selectedItem.port" />
                    </div>
                    <div class="form-group">
                        <label for="compDomain">Domain Name:</label>
                        <input type="text" class="form-control" name="domainName" id="compDomain" v-model="selectedItem.domainName" />
                    </div>
                    <div class="form-group">
                        <label for="compStatusPath">Status Path:</label>
                        <input type="text" class="form-control" name="statusPath" id="compStatusPath" v-model="selectedItem.statusPath" />
                    </div>
                    <div class="form-group">
                        <label for="compHealthPath">Health Path:</label>
                        <input type="text" class="form-control" name="healthPath" id="compHealthPath" v-model="selectedItem.healthPath" />
                    </div>
                    <div class="form-group">
                        <label for="compDescription">Description:</label>
                        <textarea type="text" class="form-control compDescription" name="description" id="compDescription" cols="10" rows="5" v-model="selectedItem.description"></textarea>
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-danger" v-show="mode == 'edit'" v-on:click="deleteItem">Delete</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
            
            <config-box v-bind:componentId="selectedItem.id" v-bind:mode="mode" v-show="mode != 'add'"></config-box>
            
            <commands-box v-bind:commands="selectedItem.commands" v-bind:componentId="selectedItem.id" v-bind:mode="mode" v-show="mode != 'add'"></commands-box>
        </div>
    </div>  
    `
});

Vue.component('health-check-box', {
    data: function() {
        return {
            responseStatus: "",
            responseBody: ""
        };
    },
    props: {
        endpoint: String
    },
    methods: {
        sendRequest: function() {
            var self = this;
            jQuery.get('/health/check?endpoint=' + this.endpoint)
                .done(function(healthCheckObj) {
                    if (healthCheckObj.error) {
                        self.responseBody = healthCheckObj.error;
                    } else {
                        self.responseStatus = healthCheckObj.status;
                        self.responseBody = healthCheckObj.body;
                    }
                })
                .fail(function( jqxhr, textStatus, error ) {
                    self.responseBody = error;
                });

            // jQuery.ajax({
            //     url: this.endpoint,
            //     dataType: 'jsonp',
            //     crossDomain: true,
            //     success: function(data) {
            //         console.log('success - data:');
            //         console.log(data);
            //         self.responseBody = data;
            //     },
            //     error: function(jqxhr, textStatus, error) {
            //         console.log('jqxhr:');
            //         console.log(jqxhr);
            //         console.log('textStatus:');
            //         console.log(textStatus);
            //         console.log('error:');
            //         console.log(error);
            //
            //         self.responseBody = error;
            //     }
            // });
        }
    },
    template: `
        <div>
            <p>
                <a v-bind:href="endpoint" class="card-link" target="_blank">{{ endpoint }}</a>
                <button type="button" class="btn btn-sm btn-outline-dark" v-on:click="sendRequest">check</button>
            </p>
            <dl>
                <dt>Status:</dt>
                <dd>{{ responseStatus }}</dd>    
                <dt>Body:</dt>
                <dd>
                    <code>{{ responseBody }}</code>
                </dd>
            </dl>        
        </div>
    `
});


Vue.component('config-box', {
    data: function() {
        return {
            files: []
        };
    },
    props: {
        componentId: 0,
        mode: "view"
    },
    watch: {
        componentId: function(newVal, oldVal) {
            if (newVal) {
                this.listFiles();
            }
        }
    },
    methods: {
        listFiles: function() {
            var self = this;
            jQuery.get('/configfiles/list?componentId=' + this.componentId).done(function(data) {
                self.files = data ? data : [];
            }).fail(function() {
                self.files = [];
            });
        },
        uploadFile: function(event) {
            event.preventDefault();
            var self = this;

            var form = jQuery('#configUploadForm')[0];
            var data = new FormData(form);

            jQuery.ajax({
                url: '/configfiles/save?componentId=' + this.componentId,
                enctype: 'multipart/form-data',
                type: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                success: function(configFile) {
                    var idx = self.findIndexByFileName(configFile.name);
                    if (idx == -1) {
                        self.files.push(configFile);
                    }
                },
                error: function(err) {
                    console.log('/configfiles/save err:');
                    console.log(err);
                }
            });
        },
        deleteFile: function(fileName) {
            var self = this;

            jQuery.ajax({
                url: '/configfiles/delete?componentId=' + this.componentId + '&fileName=' + fileName,
                type: 'DELETE',
                success: function(data) {
                    var idx = self.findIndexByFileName(fileName);
                    if (idx >= 0) {
                        self.files.splice(idx, 1);
                    }
                },
                error: function(err) {
                    console.log('/configfiles/delete err:');
                    console.log(err);
                }
            });
        },
        findIndexByFileName: function(fileName) {
            var len = this.files.length;
            if (len) {
                for (var idx = 0; idx<len; idx++) {
                    if (this.files[idx] == fileName) {
                        return idx;
                    }
                }
            }
            return -1;
        }
    },
    template: `
    <div>
        <div class="card mb-3" v-show="mode == 'view' && files.length">
            <div class="card-header">Configuration</div>
            <div class="card-body">
                <ul>
                    <li v-for="configFile in files" >
                        <a v-bind:href="'/static/files/' + configFile.componentId + '/' + configFile.name" target="_blank">{{ configFile.name }}</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="card mb-3" v-show="mode != 'view'">
            <div class="card-header">Configuration</div>
            <div class="card-body">
                <ul>
                    <li v-for="configFile in files" >
                        <a v-bind:href="'/static/files/' + configFile.componentId + '/' + configFile.name" target="_blank">{{ configFile.name }}</a>
                        <button type="button" class="btn btn-sm btn-outline-danger" v-on:click="deleteFile(configFile.name)">X</button>
                    </li>
                </ul>
                <form id="configUploadForm" method="post" enctype="multipart/form-data" v-on:submit="uploadFile">
                    <input type="file" name="configfile" />
                    <button type="submit" class="btn btn-sm btn-primary">Upload</button>
                </form>
            </div>
        </div>
    </div>
    `
});


Vue.component('commands-box', {
    data: function() {
        return {
            items: []
        };
    },
    props: {
        commands: [],
        componentId: 0,
        mode: "view"
    },
    watch: {
        commands: function(newVal, oldVal) {
            this.items = newVal ? newVal : [];
        }
    },
    methods: {
        saveCommand: function(cmd) {
            var self = this;

            jQuery.ajax({
                url: '/commands/save',
                type: 'POST',
                data: JSON.stringify(cmd),
                contentType: "application/json",
                dataType: 'json'
            }).done(function(command) {
                var idx = self.findIndexByProperty('value', command.value);
                if (idx > -1) {
                    self.items[idx] = command;
                }
            })
        },
        deleteCommand: function(cmd) {
            var self = this;

            jQuery.ajax({
                url: '/commands/delete?id=' + cmd.id,
                type: 'DELETE',
                success: function(obj) {
                    var idx = self.findIndexByProperty('id', cmd.id);
                    if (idx >= 0) {
                        self.items.splice(idx, 1);
                    }
                },
                error: function() {

                }
            });
        },
        findIndexByProperty: function(prop, val) {
            var len = this.items.length;
            if (len) {
                for (var idx = 0; idx<len; idx++) {
                    if (this.items[idx][prop] == val) {
                        return idx;
                    }
                }
            }
            return -1;
        },
        addCommandTemplate: function() {
            this.items.push({
                id: 0,
                componentId: this.componentId,
                name: '',
                value: ''
            });
        },
        executeCommandMock: function(cmd) {
            var deferred = $.Deferred();
            setTimeout(function() {
                cmd.result = 'Execution Result';
                deferred.resolve(cmd);
            }, 2000);
            return deferred.promise();
        },
        executeCommand: function(cmd) {
            var self = this;

            // this.executeCommandMock(cmd).done(function(command) {
            //     var idx = self.findIndexByProperty('id', cmd.id);
            //     if (idx > -1) {
            //         self.items[idx] = command;
            //     }
            // });

            jQuery.ajax({
                url: '/commands/execute',
                type: 'POST',
                data: JSON.stringify(cmd),
                contentType: "application/json",
                dataType: 'json'
            }).done(function(command) {

                var idx = self.findIndexByProperty('id', command.id);
                if (idx > -1) {
                    Vue.set(self.items, idx, command);
                }
            });
        }
    },
    template: `
    <div>
        <div class="card" v-show="mode == 'view' && items.length">
            <div class="card-header">Commands</div>
            <div class="card-body">
                <ul>
                    <li v-for="item in items" :key="item.id">
                        <h6>
                            {{item.name}}
                            <button type="button" class="btn btn-sm btn-outline-success" v-on:click="executeCommand(item)">Execute</button>
                        </h6>
                        <span class="badge badge-secondary">{{ item.value }}</span>
                        <div>
                            <code>{{ item.result }}</code>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    
        <div class="card" v-show="mode == 'edit'">
            <div class="card-header">Commands</div>
            <div class="card-body">
                <ul>
                    <li v-for="cmd in items" :key="cmd.id">
                        <form>
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" v-model="cmd.name" placeholder="Name" />
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="value" v-model="cmd.value" placeholder="Value" />
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-sm btn-danger" v-on:click="deleteCommand(cmd)">Delete</button>
                                <button type="button" class="btn btn-sm btn-primary" v-on:click="saveCommand(cmd)">Save</button>
                            </div>
                        </form>                
                    </li>
                </ul>
                <button type="button" class="btn btn-sm btn-success" v-on:click="addCommandTemplate">Add</button>
            </div>
        </div>
    </div>
    `
});

// TODO
Vue.component('health-check-box-new', {
    data: function() {
        return {
            endpoints: [],
            responseStatus: "",
            responseBody: ""
        };
    },
    props: {
        item: Object,
        endpoint: String,
        mode: String
    },
    methods: {
        sendRequest: function() {
            var self = this;
            jQuery.get('/health/check?endpoint=' + this.endpoint)
                .done(function(healthCheckObj) {
                    if (healthCheckObj.error) {
                        self.responseBody = healthCheckObj.error;
                    } else {
                        self.responseStatus = healthCheckObj.status;
                        self.responseBody = healthCheckObj.body;
                    }
                })
                .fail(function( jqxhr, textStatus, error ) {
                    self.responseBody = error;
                });
        },
        savePath: function(path) {

        },
        deletePath: function(path) {

        },
        appendTemplate: function() {

        }
    },
    template: `
    <div class="card mb-3">
        <div class="card-header">Health-Check</div>
        <div class="card-body">
            
            <ul v-if="mode == 'view'">
                <li v-for="endpoint in endpoints">
                    <a v-bind:href="endpoint" class="card-link" target="_blank">{{ endpoint }}</a>
                    <button type="button" class="btn btn-sm btn-outline-dark" v-on:click="sendRequest">check</button>
                    
                    <dl>
                        <dt>Status:</dt>
                        <dd>{{ responseStatus }}</dd>    
                        <dt>Body:</dt>
                        <dd>
                            <code>{{ responseBody }}</code>
                        </dd>
                    </dl>
                </li>
            </ul>

            <div v-if="mode == 'edit'">
                <ul>
                    <li v-for="path in item.paths" :key="path.id">
                        <form>
                            <div class="form-group">
                                <input type="text" class="form-control" name="value" v-model="path.value" placeholder="Value" />
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-sm btn-danger" v-on:click="deletePath(path)">Delete</button>
                                <button type="button" class="btn btn-sm btn-primary" v-on:click="savePath(path)">Save</button>
                            </div>
                        </form>                
                    </li>
                </ul>
                <button type="button" class="btn btn-sm btn-success" v-on:click="appendTemplate">Add</button>
            </div>
        </div>
    </div>
    `
});
