
var mainApp = new Vue({
  el: '#main-app',
  methods: {
    onComponentsSearchEvent: function(filterObj) {
      this.$refs.componentsBox.filterItems(filterObj);
    },
    onTeamsListUpdatedEvent: function(teamsList) {
      this.$refs.envBox.updateTeamsList(teamsList);
      this.$refs.componentsSearchBox.updateTeamsList(teamsList);
    },
    onEnvListUpdatedEvent: function(envList) {
      this.$refs.componentsSearchBox.updateEnvList(envList);
    }
  }
});