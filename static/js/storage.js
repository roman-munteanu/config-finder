
var store = {
  local: {
    items: [],
    delayTime: 300,
    findAll: function() {
      var self = this;
      var deferred = $.Deferred();

      setTimeout(function() {
        // items copy: self.items.slice();
        deferred.resolve(self.items);
      }, this.delayTime);

      return deferred.promise();
    },
    findComponents: function(filterObj) {
      var self = this;
      var deferred = $.Deferred();
      var result = [];

      setTimeout(function() {
        for (var i=0; i<self.items.length; i++) {
          var item = self.items[i];
          if (item.teamId == filterObj.teamId && item.envId == filterObj.envId) {
            result[result.length] = item;
          }
        }
        deferred.resolve(result);
      }, this.delayTime);

      return deferred.promise();
    },
    save: function(it) {
      return it.id ? this.update(it) : this.add(it);
    },
    add: function(it) {
      var self = this;
      var deferred = $.Deferred();
      setTimeout(function() {
        it.id = Date.now();
        self.items.push(it);
        deferred.resolve(self.items);
      }, this.delayTime);
      return deferred.promise();
    },
    update: function(it) {
      var self = this;
      var deferred = $.Deferred();
      setTimeout(function() {
        var filteredItems = self.items.filter(item => { item.id == it.id; });
        if (filteredItems.length) {
          var index = self.items.indexOf(filteredItems[0]);
          self.items[index] = it;
        }
        deferred.resolve(self.items);
      }, this.delayTime);
      return deferred.promise();
    },
    delete: function(it) {
      var self = this;
      var deferred = $.Deferred();
      setTimeout(function() {
        self.items.splice(self.items.indexOf(it), 1);
        deferred.resolve(self.items);
      }, this.delayTime);
      return deferred.promise();
    }
  },
  server: {
    url: "",
    storeData: {
      findAllUrl: '',
      findComponentsUrl: '/components',
      saveUrl: '',
      deleteUrl: ''
    },
    items: [],
    findAll: function() {
      var self = this;
      var deferred = $.Deferred();

      jQuery.get(this.storeData.findAllUrl).done(function(data) {
        self.items = data;
      }).always(function() {
        deferred.resolve(self.items);
      });

      return deferred.promise();
    },
    findComponents: function(filterObj) {
      var self = this;
      var deferred = $.Deferred();

      jQuery.get(this.storeData.findComponentsUrl + '?teamId=' + filterObj.teamId + '&envId=' + filterObj.envId).done(function(data) {
        self.items = data;
      }).always(function() {
        deferred.resolve(self.items);
      });

      return deferred.promise();
    },
    save: function(it) {
      var self = this;
      var deferred = $.Deferred();

      console.log('save:');
      console.log(it);
      console.log(JSON.stringify(it));

      jQuery.ajax({
        url: this.storeData.saveUrl,
        type: 'POST',
        data: JSON.stringify(it),
        contentType: "application/json",
        dataType: 'json'
      }).done(function(responseItem) {
        if (it.id) {
          var idx = self.findIndexByItemId(responseItem.id);
          self.items[idx] = responseItem;
        } else {
          self.items.push(responseItem);
        }
      }).always(function() {
        deferred.resolve(self.items);
      });

      return deferred.promise();
    },
    delete: function(it) {
      var self = this;
      var deferred = $.Deferred();

      jQuery.ajax({
        url: this.storeData.deleteUrl + '?id=' + it.id,
        type: 'DELETE',
        success: function(obj) {
          var idx = self.findIndexByItemId(it.id);
          if (idx >= 0) {
            self.items.splice(idx, 1);
          }
          deferred.resolve(self.items);
        },
        error: function() {
          deferred.resolve(self.items);
        }
      });

      return deferred.promise();
    },
    findIndexByItemId: function(itemId) {
      var len = this.items.length;
      if (len) {
        for (var idx = 0; idx<len; idx++) {
          if (this.items[idx]['id'] == itemId) {
            return idx;
          }
        }
      }
      return -1;
    }
  },
  newStore: function(storeType, initialData, storeData) {
    var storage = Object.assign({}, store[storeType]);
    storage.items = initialData || [];
    storage.storeData = storeData || storeData;
    return storage;
  }
};

var initialData = {
  "teams": [
    {"id": 1, "name": "Team A"},
    {"id": 2, "name": "Team B"},
    {"id": 3, "name": "Team C"}
  ],
  "environments": [
    {"id": 1, "teamId": 1, "name": "DEV"},
    {"id": 2, "teamId": 1, "name": "QA"},
    {"id": 3, "teamId": 1, "name": "UAT"},
    {"id": 4, "teamId": 1, "name": "PROD"},
    {"id": 5, "teamId": 2, "name": "DEV"},
    {"id": 6, "teamId": 2, "name": "QA"},
    {"id": 7, "teamId": 3, "name": "DEV"}
  ],
  "components": [
      {
        "id": 1,
        "teamId": 1,
        "envId": 1,
        "name": "Project A",
        "protocol":"http",
        "port":"8181",
        "ipAddress":"127.0.0.1",
        "domainName":"localhost",
        "statusPath":"/api/actuator/health",
        "healthPath":"/actuator/health",
        "description":"Project A Jenkins URL: Documentation:"
      },
      {
        "id": 2,
        "teamId": 1,
        "envId": 2,
        "name": "Project B",
        "protocol":"https",
        "port":"8443",
        "ipAddress":"127.0.0.1",
        "domainName":"b.domain.com",
        "statusPath":"/b/status",
        "healthPath":"/b/health",
        "description":"Project B Jenkins URL: Documentation:"
      },
      {
        "id": 3,
        "teamId": 2,
        "envId": 5,
        "name": "Project C",
        "protocol":"http",
        "port":"8484",
        "ipAddress":"192.168.0.1",
        "domainName":"c.domain.com",
        "statusPath":"/c/status",
        "healthPath":"/c/health",
        "description":"Project C Jenkins URL: Documentation:"
      }
  ]
};