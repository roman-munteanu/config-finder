Vue.component('env-box', {
  data: function() {
    return {
      items: [],
      selectedItem: {},
      mode: "edit",
      teams: [],
      selectedTeam: {}
    };
  },
  teamsStore: {},
  envStore: {},
  created: function() {
    var self = this;
    // this.envStore = store.newStore('local', initialData['environments']);

    var storeData = {
      findAllUrl: '/environments',
      saveUrl: '/environments/save',
      deleteUrl: '/environments/delete'
    };
    this.envStore = store.newStore('server', null, storeData);

    this.envStore.findAll().done(function(data) {
      self.items = data;
      self.broadcastEnvListUpdated();
    });
  },
  methods: {
    switchMode: function(mode) {
      this.mode = mode;
      if (mode == 'add') {
        this.selectedItem = {};
      }
    },
    showItem: function(it) {
      this.selectedItem = it;
    },
    saveItem: function(event) {
      event.preventDefault();
      var self = this;

      var it = this.selectedItem;
      var name = it.name;
      if (name && name.trim()) {
        var item = {
          teamId: this.selectedTeam.id,
          name: name.trim()
        };
        if (it.id) item.id = it.id;

        this.envStore.save(item).done(function(data) {
          self.broadcastEnvListUpdated();
        });
      }
    },
    deleteItem: function() {
      var self = this;
      this.envStore.delete(this.selectedItem).done(function(data) {
        self.selectedItem = {};
        self.broadcastEnvListUpdated();
      });
    },
    searchEnvs: function(event) {
      event.preventDefault();
      this.filterItems();
    },
    filterItems: function() {
      var self = this;
      return this.items.filter(function(item) {
        return item.teamId == self.selectedTeam.id;
      });
    },
    updateTeamsList: function(teamsList) {
      this.teams = teamsList;
    },
    broadcastEnvListUpdated: function() {
      this.$emit('env-list-updated-event', this.items);
    }
  },
  computed: {
    setSelectedTeam: {
      get: function() {
        return this.selectedTeam.id;
      },
      set: function(optionValue) {
        var teamsResult = this.teams.filter(t => t.id == optionValue);
        this.selectedTeam = teamsResult.length ? teamsResult[0] : {environments: []};
      }
    },
    filteredItems: function() {
      return this.filterItems();
    }
  },
  template: `
  <div>
        <div class="row mb-4">
          <form class="form-inline" v-on:submit="searchEnvs">
              <label class="my-1 mr-2" for="inlineFormTeam">Team:</label>
              <select name="teamId" id="inlineFormTeam" class="form-control my-1 mr-sm-2 mr-3" v-model="setSelectedTeam">
                  <option v-for="team in teams" :key="team.id" :value="team.id">{{ team.name }}</option>
              </select>
          
              <button type="submit" class="btn btn-primary my-1">Search</button>
          </form>
        </div> 
        <div class="row mb-4">
            <div class="col-4">
                <ul class="list-group">
                    <li class="list-group-item" v-for="item in filteredItems" :key="item.id" v-bind:class="{ active: item.id == selectedItem.id }" v-on:click="showItem(item)">
                        {{ item.name }}
                    </li>
                </ul>
            </div>
            
            <div class="col-8">
                <div class="btn-group btn-group-sm" role="group">
                    <button type="button" class="btn btn-secondary" v-bind:class="{ active: mode == 'edit' }" v-on:click="switchMode('edit')">Edit</button>
                    <button type="button" class="btn btn-secondary" v-bind:class="{ active: mode == 'add' }" v-on:click="switchMode('add')">Add</button>
                </div>
            
                <div class="card" id="envEditMode">
                    <form class="card-body" v-on:submit="saveItem">
                        <input type="hidden" name="id" value="selectedItem.id" />
                        <div class="form-group">
                            <label for="envName">Name*:</label>
                            <input type="text" class="form-control" name="name" id="envName" v-model="selectedItem.name" />
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-danger" v-show="mode == 'edit'" v-on:click="deleteItem">Delete</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    `
});