Vue.component('teams-box', {
    data: function() {
        return {
            items: [],
            selectedItem: {},
            mode: "edit"
        };
    },
    teamsStore: {},
    created: function() {
        var self = this;
        // this.teamsStore = store.newStore('local', initialData['teams']);

        var storeData = {
            findAllUrl: '/teams',
            saveUrl: '/teams/save',
            deleteUrl: '/teams/delete'
        };
        this.teamsStore = store.newStore('server', null, storeData);

        this.teamsStore.findAll().done(function(data) {
            self.items = data;
            self.broadcastTeamsListUpdated();
        });
    },
    methods: {
        switchMode: function(mode) {
            this.mode = mode;
            if (mode == 'add') {
                this.selectedItem = {};
            }
        },
        showItem: function(it) {
            this.selectedItem = it;
        },
        saveItem: function(event) {
            event.preventDefault();
            var self = this;

            var it = this.selectedItem;
            var name = it.name;
            if (name && name.trim()) {
                var item = {
                    name: name.trim()
                };
                if (it.id) item.id = it.id;

                this.teamsStore.save(item).done(function(data) {
                    self.broadcastTeamsListUpdated();
                });
            }
        },
        deleteItem: function() {
            var self = this;
            this.teamsStore.delete(this.selectedItem).done(function(data) {
                self.selectedItem = {};
                self.broadcastTeamsListUpdated();
            });
        },
        broadcastTeamsListUpdated: function() {
            this.$emit('teams-list-updated-event', this.items);
        }
    },
    template: `
        <div class="row mb-4">
            <div class="col-4">
                <ul class="list-group">
                    <li class="list-group-item" v-for="item in items" :key="item.id" v-bind:class="{ active: item.id == selectedItem.id }" v-on:click="showItem(item)">
                        {{ item.name }}
                    </li>
                </ul>
            </div>
            
            <div class="col-8">
                <div class="btn-group btn-group-sm" role="group">
                    <button type="button" class="btn btn-secondary" v-bind:class="{ active: mode == 'edit' }" v-on:click="switchMode('edit')">Edit</button>
                    <button type="button" class="btn btn-secondary" v-bind:class="{ active: mode == 'add' }" v-on:click="switchMode('add')">Add</button>
                </div>
            
                <div class="card" id="teamEditMode">
                    <form class="card-body" v-on:submit="saveItem">
                        <input type="hidden" name="id" value="selectedItem.id" />
                        <div class="form-group">
                            <label for="teamName">Name*:</label>
                            <input type="text" class="form-control" name="name" id="teamName" v-model="selectedItem.name" />
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-danger" v-show="mode == 'edit'" v-on:click="deleteItem">Delete</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    `
});